FROM yobasystems/gitlab-runner

ARG VERSION

#Python 3.7 install
ENV BASE_URL="https://get.helm.sh"
ENV TAR_FILE="helm-v${VERSION}-linux-arm.tar.gz"

RUN apk add --update --no-cache curl ca-certificates && \
    curl -L ${BASE_URL}/${TAR_FILE} |tar xvz && \
    mv linux-arm/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-arm && \
    apk del curl && \
    rm -f /var/cache/apk/*

RUN apk add curl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.19.0/bin/linux/arm/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
RUN apk add gettext


WORKDIR /apps

ENTRYPOINT [""]
